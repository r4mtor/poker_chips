echo(version=version());

font = "Liberation Sans";

cylinder_height = 4;
cylinder_size = 35;


module letter(l, letter_size) {
	linear_extrude(height = 2) {
		text(l, size = letter_size, font = font, halign = "center", valign = "center", $fn = 100);
	}
}

module chip(x, y, l, letter_size = 18) {
    difference() {
        translate([x,y,0]) color("green") cylinder(h=cylinder_height, d= cylinder_size, center = true, $fn = 100);        
        translate([x,y,2.5]) cylinder(h=cylinder_height, d= cylinder_size-5, center = true,$fn = 100);    
    }
    translate([x, y, -0.2]) rotate([0, 0, 0]) letter(l, letter_size);
}

module coffee_chip(x, y) {
    difference() {
        translate([x,y,0]) color("green") cylinder(h=cylinder_height, d= cylinder_size, center = true, $fn = 100);        
        translate([x,y,2.5]) cylinder(h=cylinder_height, d= cylinder_size-5, center = true,$fn = 100);    
    }
    translate([x, y, -0.2]) {
        hull(){
            translate([0,2,1]) cube([13,13,2], center=true);
            translate([-4,-5,1])cylinder(h=2,d=5,center=true,$fn = 100);
            translate([4,-5,1])cylinder(h=2,d=5,center=true,$fn = 100);
        }
        translate([7,2,1])
        difference(){
            cylinder(h=2,d=7,center=true,$fn = 100);
            cylinder(h=4,d=5,center=true,$fn = 100);        translate([-7,0,0]) cube([12,15,4], center=true);
        }
    }
}

chip(  0,  0,   "0");
chip( 40,  0,   "1");
chip( 80,  0,   "2");
chip(120,  0,   "3");
chip(  0, 40,   "5");
chip( 40, 40,   "8");
chip( 80, 40,  "13");
chip(120, 40,  "20");
chip(  0, 80,  "40");
chip( 40, 80, "100", 12);
chip( 80, 80,   "?");
coffee_chip(120, 80);
                