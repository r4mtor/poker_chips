# Scrum Poker Chips

This is a simple [openscad](www.openscad.org) project, that contains scrum poker chips to estimate tasks and stories in the agile workflow.

The following chips are designed in this [file](poker_chip.scad)
 - 0 h
 - 1 h
 - 2 h
 - 3 h
 - 5 h
 - 8 h
 - 13 h
 - 20 h
 - 40 h
 - 100 h
 - ?
 - Coffee pod
 
 Have look at rendered picture.
![test](poker_chip.png)